package scripts.tribotConsoleResizer;

import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import scripts.fluffeesapi.client.ClientUtilities;

@ScriptManifest(
        authors = "Fluffee",
        category = "Tools",
        name = "TRiBot Console Resizer",
        description = "Local version.",
        version = 1,
        gameMode = 1)

public class Main extends Script {
    @Override
    public void run() {
        ClientUtilities.resizeSplitPane(0.75);
    }
}
